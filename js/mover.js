document.addEventListener('DOMContentLoaded', function () {
    const imagenActual = document.getElementById('imagen-en-movimiento');
    const imagenes = ['/img/pennybanner.jpg', '/img/skatebanner.jpg', '/img/palacebanner.jpg']; // Agrega las rutas de tus imágenes
    let currentIndex = 0;

    function cambiarImagen() {
        currentIndex = (currentIndex + 1) % imagenes.length;
        const nuevaImagen = imagenes[currentIndex];

        imagenActual.style.animation = 'desplazar-izquierda 1s forwards';

        setTimeout(function () {
            // Actualiza la imagen y reinicia la animación
            imagenActual.src = nuevaImagen;
            imagenActual.style.animation = '';
        }, 500); // Espera 1 segundo para la transición
    }

    // Inicia el intervalo para cambiar la imagen cada 5 segundos
    setInterval(cambiarImagen, 5000); // 5000 milisegundos (5 segundos)
});